package holders

import (
	"buildable/dbase/mongo"
	"buildable/dbase"
	"buildable/models"
	"buildable/services/builder"
	"buildable/services/builder/golang"
	osService "buildable/services/os"
	"github.com/labstack/gommon/log"
	"runtime"
	"buildable/services/os/debian"
	"buildable/services/builder/nodejs"
)

var Project *models.Project = nil

func Database() (dbase.Database, error) {
	return mongo.NewMongoDatabase("mongodb://admin:admin228@ds245971.mlab.com:45971/buildable")
}

func Builder(p *models.Project, service osService.OS) builder.Builder {
	if p.Env.Type == "go" {
		return golang.NewGolangBuilder(p, service)
	} else if p.Env.Type == "nodejs" {
		return nodejs.NewNodeBuilder(p, service)
	}
	panic("not implemented yet: " + p.Env.Type)
}

func NewOSSession() osService.OS {
	log.Info("Current system: " + runtime.GOOS)
	return debian.NewDebian()
}