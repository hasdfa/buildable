package server

import (
	"github.com/labstack/echo"
	"buildable/server/routes"
	"buildable/utils/bitbucket"
	"github.com/labstack/gommon/log"
	"net/http"
)

type CustomServer struct {
	port string
	server *echo.Echo
}

const verySecretKey = "hasdfa"

func (s *CustomServer) StartServer() {
	//s.server = echo.New()
	//s.server.Use(middleware.Logger())
	//s.server.Use(func(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	//	return func(context echo.Context) error {
	//		// security ;)
	//		key := context.QueryParam("key")
	//		if key != verySecretKey {
	//			return echo.NotFoundHandler(context)
	//		}
	//
	//		db, err := holders.Database()
	//		if err != nil { return err }
	//		context.Set("dbase", db)
	//		return handlerFunc(context)
	//	}
	//})

	wh := bitbucket.NewWebhook()
	wh.Handle("repo:push", routes.HandleTrigger)

	http.HandleFunc("/api/v1/triggers/", wh.ServeHTTP)
	log.Fatal(http.ListenAndServe(s.port, nil))
}

func (s *CustomServer) StopServer() error {
	return s.server.Close()
}
