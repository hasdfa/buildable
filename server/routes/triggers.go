package routes

import (
	"buildable/dbase"
	"net/http"
	"buildable/models/project"
	"buildable/utils/bitbucket"
	"buildable/services/engine/custom"
	"buildable/holders"
	"strings"
	"errors"
)

func HandleTrigger(r *http.Request, headers bitbucket.Headers, _event interface{}) error {
	event := _event.(*bitbucket.RepoPushEvent)
	db, err := holders.Database()
	if err != nil { return err }

	pathes := strings.Split(r.URL.Path, "/")
	projectName := pathes[len(pathes) - 1]

	var dbProject project.DBProject
	err = dbase.ProjectsCollection(db).FindId(projectName).One(&dbProject)
	if err != nil {
		return errors.New("project was not found")
	}
	//if is, ok := custom.RunningInstances.Load(dbProject.Name); ok && is.(bool) {
	//	return errors.New("project already running")
	//}

	starter := custom.NewCustomEngine(&dbProject, headers, event)
	go starter.StartAsync()

	return nil
}
