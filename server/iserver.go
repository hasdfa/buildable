package server

type Server interface {
	StartServer()
	StopServer() error
}

func NewServer() Server {
	return &CustomServer{
		port: ":5890",
	}
}