package utils

import "os"

func FileNotExist(path string) bool {
	_, err := os.Stat(path)
	return os.IsNotExist(err)
}
