package project

type DBProject struct {
	Id     string  `bson:"_id"`
	Name   string `bson:"name"`
	GitUrl string `bson:"git"`
}
