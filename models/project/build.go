package project

import (
	"buildable/models/pipelines"
	"buildable/utils/bitbucket"
)

type Build struct {
	Uid string `bson:"_id"`
	//Request struct {
	//	Headers bitbucket.Headers
	//	Event   *bitbucket.RepoPushEvent
	//} `bson:"request"`
	Actor bitbucket.Actor `bson:"actor"`
	Branch string `bson:"branch"`

	Error error `bson:"error"`
	ErrorStr string `bson:"errorString"`

	Reports      []*pipelines.PipelineReport `bson:"reports"`
	DeployReport *pipelines.PipelineReport   `bson:"deployReport"`
}
