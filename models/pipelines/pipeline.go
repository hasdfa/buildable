package pipelines

import "time"

type PipelineFile struct {
	Name         string            `yaml:"-"            json:"name"`

	Env          map[string]string `yaml:"env"          json:"env"`
	Requirements []string          `yaml:"requirements" json:"requirements"`

	Pipeline     []*struct{
		Step     	*Step          `yaml:"step" json:"step"`
	}  `yaml:"pipeline"     json:"pipeline"`

	Report       *PipelineReport   `yaml:"-"             json:"report"`
}

type Step struct {
	Name   string   `yaml:"name"   json:"name"`
	Script []string `yaml:"script" json:"script"`
}


type PipelineReport struct {
	Key       string        `json:"file"`
	Timestamp time.Time     `json:"timestamp"`
	Steps     []*StepReport `json:"steps"`

	Error     error         `json:"error"`
}

type StepReport struct {
	Step  *Step    `json:"step"`

	Error   error  `json:"error"`
	ErrStr  string `json:"errString"`
	ErrLine int    `json:"errLine"`
}