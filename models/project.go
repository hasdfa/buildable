package models

import (
	"io/ioutil"
	"encoding/json"
	"path"
	"os"
	"sync"
	"buildable/models/pipelines"
)

type Project struct {
	Uid       string                    `json:"-"`
	Name      string                    `json:"-"`
	Env       Environment               `json:"env"`

	Branches  []BranchConfig            `json:"branches"`

	Pipelines sync.Map                  `json:"-"`
	Deploy    *pipelines.PipelineFile   `json:"-"`
}

type Environment struct {
	Type    string `json:"type"`
	Version string `json:"version"`
	Package string `json:"package"`
}

type BranchConfig struct {
	Name      string   `json:"name"`

	Pipelines []string `json:"pipelines"`
	Deploy    string   `json:"deploy"`
}

func (p *Project) binPackage() string {
	if p.Env.Type == "go" {
		return p.Env.Type + p.Env.Version
	}
	return ""
}

func (p *Project) Path() string {
	return path.Join(os.Getenv("HOME"), "/buildable/projects", p.Name, p.Uid)
}

func (p *Project) Bin() string {
	return path.Join(os.Getenv("HOME"), "/buildable/bin/", p.binPackage())
}

func (p *Project) Temp() string {
	return path.Join(os.Getenv("HOME"), "/buildable/temp/", p.Name, p.Uid)
}

func NewProject(file string) (*Project, error) {
	bytes, err := ioutil.ReadFile(file)
	if err != nil { return nil, err }

	var proj Project
	err = json.Unmarshal(bytes, &proj)
	if err != nil { return nil, err }

	os.MkdirAll(proj.Path(), os.ModeDir)
	os.MkdirAll(proj.Temp(), os.ModeDir)
	return &proj, nil
}