package dbase

type queryType int

const (
	typeFind queryType = iota
	typeInsert
	typeUpdate
	typeDelete
)

type Query interface {
	Skip(n int) Query
	Limit(n int) Query

	Select(selector []string) Query
	Sort(fields ...string) Query

	One(result interface{}) error
	All(result interface{}) error
	Count() (n int, err error)
}

//func (q *Query) Skip(n int) *Query {
//	q.m.Lock()
//	q.op.skip = int32(n)
//	q.m.Unlock()
//	return q
//}
//
//type queryOp struct {
//	collection string
//	query      interface{}
//	skip       int32
//	limit      int32
//	selector   interface{}
//
//	queryType  queryType
//}
