package dbase

type Database interface {
	Collections() ([]string, error)
	Collection(name string) Collection
	CreateCollection(string) error
}