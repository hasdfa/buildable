package dbase

type Collection interface {
	Find(args map[string]interface{}) Query
	FindId(query string) Query

	Insert(docs ...interface{}) error

	Update(selector interface{}, update interface{}) error
	UpdateId(id string, update interface{}) error
	UpdateAll(selector interface{}, update interface{}) error

	Remove(selector interface{}) error
	RemoveId(id string) error

	DropCollection() error
}

func ProjectsCollection(db Database) Collection {
	return db.Collection("projects")
}

func BuildsCollection(db Database) Collection {
	return db.Collection("builds")
}