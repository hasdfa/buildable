package mongo

import (
	"buildable/dbase"
	"gopkg.in/mgo.v2"
)

type mgoCollection struct {
	collection *mgo.Collection
}

func (c *mgoCollection) Find(query map[string]interface{}) dbase.Query {
	return &mgoQuery{c.collection.Find(query)}
}

func (c *mgoCollection) FindId(id string) dbase.Query {
	return &mgoQuery{c.collection.FindId(id)}
}

func (c *mgoCollection) Insert(docs ...interface{}) error {
	return c.collection.Insert(docs...)
}

func (c *mgoCollection) Update(selector interface{}, update interface{}) error {
	return c.collection.Update(selector, update)
}

func (c *mgoCollection) UpdateId(id string, update interface{}) error {
	return c.collection.UpdateId(id, update)
}

func (c *mgoCollection) UpdateAll(selector interface{}, update interface{}) error {
	_, err := c.collection.UpdateAll(selector, update)
	return err
}

func (c *mgoCollection) Remove(selector interface{}) error {
	return c.collection.Remove(selector)
}

func (c *mgoCollection) RemoveId(id string) error {
	return c.collection.RemoveId(id)
}

func (c *mgoCollection) DropCollection() error {
	return c.collection.DropCollection()
}

