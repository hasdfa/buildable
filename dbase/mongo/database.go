package mongo

import (
	"gopkg.in/mgo.v2"
	"buildable/dbase"
)

type mgoDatabase struct {
	db *mgo.Database
}

func NewMongoDatabaseWithInfo(info *mgo.DialInfo) (dbase.Database, error) {
	sess, err := mgo.DialWithInfo(info)
	if err != nil { return nil, err }
	return &mgoDatabase{sess.DB("buildable")}, nil
}

func NewMongoDatabase(url string) (dbase.Database, error) {
	sess, err := mgo.Dial(url)
	if err != nil { return nil, err }
	return &mgoDatabase{sess.DB("buildable")}, nil
}

func (db *mgoDatabase) Collections() ([]string, error) {
	return db.db.CollectionNames()
}

func (db *mgoDatabase) Collection(name string) dbase.Collection {
	return &mgoCollection{db.db.C(name)}
}

func (db *mgoDatabase) CreateCollection(name string) error {
	return db.db.C(name).Create(&mgo.CollectionInfo{})
}