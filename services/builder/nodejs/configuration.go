package nodejs

import (
	"path"
	"os"
)

func (b *nodeBuilder) Configure() error {
	gopath := b.project.Path()

	projPath := path.Join(gopath, "src", b.project.Env.Package)
	err := os.MkdirAll(projPath, os.ModeDir)
	if err != nil { return err }

	return nil
}