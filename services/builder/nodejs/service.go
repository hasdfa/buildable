package nodejs

import (
	"buildable/models"
	"buildable/services/builder"
	osService "buildable/services/os"
	"path"
	"fmt"
)

type nodeBuilder struct {
	osService osService.OS
	project   *models.Project
}

func NewNodeBuilder(proj *models.Project, service osService.OS) builder.Builder {
	return &nodeBuilder{service, proj}
}

func (b *nodeBuilder) Build() error {
	projectPath := path.Join(b.project.Path(), "src", b.project.Env.Package)
	fmt.Println("Path:", projectPath)

	//_, err := b.osService.ExecInDir(projectPath, "npm", "install")
	//if err != nil { return err }

	return nil
}

func (b *nodeBuilder) BinPath() string {
	return path.Join("npm")
}