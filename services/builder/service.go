package builder

type Builder interface {
	Configure() error
	Build() error
	BinPath() string
}