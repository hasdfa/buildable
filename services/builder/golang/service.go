package golang

import (
	"buildable/models"
	"buildable/services/builder"
	osService "buildable/services/os"
	"path"
	"fmt"
)

type goBuilder struct {
	osService osService.OS
	project   *models.Project
}

func NewGolangBuilder(proj *models.Project, service osService.OS) builder.Builder {
	return &goBuilder{service, proj}
}

func (b *goBuilder) Build() error {
	projectPath := path.Join(b.project.Path(), "src", b.project.Env.Package)
	fmt.Println("Path:", projectPath)

	_, err := b.osService.ExecInDir(projectPath, b.BinPath(), "get", "-v")
	if err != nil { return err }

	//_, err = b.osService.ExecInDir(projectPath, b.BinPath(), "build")
	//if err != nil { return err }

	return nil
}

func (b *goBuilder) BinPath() string {
	return path.Join(b.project.Bin(), "go/bin/go")
}