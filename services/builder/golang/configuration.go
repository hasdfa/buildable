package golang

import (
	"path"
	"os"
	"fmt"
	"runtime"
)

func (b *goBuilder) Configure() error {
	gopath := b.project.Path()
	err := os.Setenv("GOPATH", gopath)
	if err != nil { return err }

	projPath := path.Join(gopath, "src", b.project.Env.Package)
	err = os.MkdirAll(projPath, os.ModeDir)
	if err != nil { return err }

	os.Setenv("GOOS", runtime.GOOS)
	os.Setenv("GOARCH", runtime.GOARCH)

	if _, err := os.Stat(b.project.Bin()); os.IsNotExist(err) {
		file := fmt.Sprintf("go%s.linux-amd64.tar.gz", b.project.Env.Version)

		if err = os.MkdirAll(b.project.Temp(), os.ModeDir); err != nil {
			return err
		}
		if err = os.MkdirAll(b.project.Bin(), os.ModeDir); err != nil {
			return err
		}

		_, err := b.osService.ExecInDir(b.project.Temp(),
			"wget",
			"https://dl.google.com/go/" + file,
		)
		if err != nil { return err }

		_, err = b.osService.ExecInDir(b.project.Temp(),"tar", "-xvf", file)
		if err != nil { return err }

		_, err = b.osService.ExecInDir(b.project.Temp(),"rm", file)
		if err != nil { return err }

		_, err = b.osService.Exec("mv", path.Join(b.project.Temp(), "go"), path.Join(b.project.Bin()))
		if err != nil { return err }
	}
	b.osService.SetCustomRunnable("go", b.BinPath())
	return nil
}
