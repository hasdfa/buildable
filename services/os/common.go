package os

import (
	"strings"
	"buildable/utils"
)

type OS interface {
	Install(string) ([]byte, error)
	Exec(run string, args... string) ([]byte, error)
	ExecInDir(dir, run string, args... string) ([]byte, error)

	SetEnv(string, string) error
	GetEnv(string) string
	PrepareTempEnv() error

	SetCustomRunnable(string, string)
}

func ParseEnvs(os OS, str string) string {
	var keys []string
	original := str

	count := strings.Count(str, "${")
	for i := 0; i < count; i++ {
		ind := strings.Index(str, "${") + 2
		last := strings.Index(str, "}")
		key := str[ind:last]
		str = str[last + 1:]
		if !utils.ContainsString(keys, key) {
			keys = append(keys, key)
		}
	}

	for _, k := range keys {
		original = strings.Replace(original, "${"+k+"}", os.GetEnv(k), 1)
	}
	return original
}