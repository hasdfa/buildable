package debian

import (
	osService "buildable/services/os"
	"os"
	"os/exec"
	"github.com/pborman/uuid"
	"fmt"
	"sync"
	"strings"
)

type debian struct {
	uuid string
	commands sync.Map
}

func NewDebian() osService.OS {
	return &debian{
		uuid: uuid.NewRandom().String(),
	}
}

func (self *debian) SetEnv(k, v string) error {
	return os.Setenv(fmt.Sprintf("%s_BUILDABLE_%s", self.uuid, k), v)
}

func (self *debian) GetEnv(k string) string {
	return os.Getenv(fmt.Sprintf("%s_BUILDABLE_%s", self.uuid, k))
}

func (self *debian) PrepareTempEnv() error {
	prefix := self.uuid + "_BUILDABLE_"
	for _, env := range os.Environ() {
		if strings.HasPrefix(env, prefix) {
			e := strings.Split(env, "=")
			if s := self.GetEnv(e[0]); s == "" {
				err := self.SetEnv(e[0], e[1])
				if err != nil { return err }
			}
		}
	}
	return nil
}

func (self *debian) Exec(run string, args... string) ([]byte, error) {
	return self.ExecInDir("", run, args...)
}

func (self *debian) ExecInDir(dir, run string, args... string) ([]byte, error) {
	if run == "/bin/bash" {
		a := args[0]
		if r, ok := self.commands.Load(a); ok {
			run = r.(string)
			args = args[1:]
		} else if args[0] == "export" {
			spl := strings.Split(args[1], "=")
			return make([]byte, 0), self.SetEnv(spl[0], spl[1])
		}
	} else {
		if r, ok := self.commands.Load(run); ok {
			run = r.(string)
		}
	}

	fmt.Println("-----------")
	fmt.Println("Run: ", dir, run, args)
	fmt.Println("-----------")

	cmd := exec.Command(run, args...)
	if dir != "" {
		cmd.Dir = dir
	}

	out, err := cmd.CombinedOutput()
	fmt.Println(run + ":", string(out), err)
	return out, err
}

func (self *debian) Install(pkg string) ([]byte, error) {
	return self.Exec("apt-get", "-y", "install", pkg)
}

func (self *debian) SetCustomRunnable(k, v string) {
	self.commands.Store(k, v)
}