package engine

type Engine interface {
	StartAsync()
	ReportError(error)

	Clone() error
	ReadProject() error
}
