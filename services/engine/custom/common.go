package custom

import (
	"buildable/models"
	"buildable/models/project"
	"buildable/utils/bitbucket"
	osService "buildable/services/os"
	"path"
	"os"
	"errors"
	"github.com/labstack/gommon/log"
	"buildable/dbase"
	"buildable/holders"
	"io/ioutil"
	"gopkg.in/yaml.v2"
	"buildable/models/pipelines"
	"buildable/services/engine"
	"time"
	"strings"
	"fmt"
	"buildable/services/builder"
)

//var RunningInstances sync.Map

type CustomStarter struct {
	dbProj *project.DBProject
	headers bitbucket.Headers
	event   *bitbucket.RepoPushEvent
	base    string

	builder builder.Builder
	session osService.OS
	proj    *models.Project
}

func NewCustomEngine(dbProj *project.DBProject, headers bitbucket.Headers, event *bitbucket.RepoPushEvent) engine.Engine {
	return &CustomStarter{dbProj: dbProj, headers: headers, event: event, session: holders.NewOSSession() }
}

func (st *CustomStarter) StartAsync() {
	//RunningInstances.Store(st.dbProj.Name, true)
	//defer RunningInstances.Store(st.dbProj.Name, false)
	//start := time.Now()
	defer func() {
		if r := recover(); r != nil {
			st.ReportError(fmt.Errorf("%s", r))
		}
	}()

	err := st.Clone()
	if err != nil {
		st.ReportError(err); return
	}
	if err = st.ReadProject(); err != nil {
		st.ReportError(err); return
	}

	st.builder = holders.Builder(st.proj, st.session)

	if err = st.builder.Configure(); err != nil {
		st.ReportError(err); return
	}
	if err = st.builder.Build(); err != nil {
		st.ReportError(err); return
	}
	if err = st.session.PrepareTempEnv(); err != nil {
		st.ReportError(err); return
	}
	if err = st.session.SetEnv("BUILDABLE_PROJECT_DIR", path.Join(st.base, st.proj.Name)); err != nil {
		st.ReportError(err); return
	}
	if err = st.session.SetEnv("CI_BRANCH", st.event.Push.Changes[0].Old.Name); err != nil {
		st.ReportError(err); return
	}

	var reports []*pipelines.PipelineReport

	err = nil
	st.proj.Pipelines.Range(func(k, v interface{}) bool {
		report := runPipeline(st, v)
		reports = append(reports, report)
		return true
	})
	if err != nil {
		st.ReportError(err); return
	}

	deployReport := runPipeline(st, st.proj.Deploy)

	db, err := holders.Database()
	if err != nil { log.Error(err); return }
	builds := dbase.BuildsCollection(db)

	build := &project.Build{
		Uid: st.headers.RequestUid,
		Actor: st.event.Actor,
		Branch: st.event.Push.Changes[0].Old.Name,

		Reports: reports,
		DeployReport: deployReport,
	}
	err = builds.Insert(build)
	if err != nil { log.Error(err) }

}

func runPipeline(st *CustomStarter, v interface{}) *pipelines.PipelineReport {
	value := v.(*pipelines.PipelineFile)
	var err error

	report := &pipelines.PipelineReport{ Key: value.Name, Timestamp:time.Now() }

	for k, v := range value.Env {
		if err = st.session.SetEnv(k, v); err != nil {
			report.Error = err
			return report
		}
	}
	for _, r := range value.Requirements {
		if _, err = st.session.Install(r); err != nil {
			report.Error = err
			return report
		}
	}
	for _, step := range value.Pipeline {
		stepReport := &pipelines.StepReport{ Step: step.Step }
		report.Steps = append(report.Steps, stepReport)

		for i, cmd := range step.Step.Script {
			cmds := strings.Split(cmd, " ")
			run := cmds[0]
			cmds = cmds[1:]
			for i, c := range cmds {
				cmds[i] = osService.ParseEnvs(st.session, c)
			}

			if _, err = st.session.ExecInDir(
				path.Join(st.base, st.dbProj.Name),
				run, cmds...,
			); err != nil {
				stepReport.Error = err
				stepReport.ErrStr = err.Error()
				stepReport.ErrLine = i
				return report
			}
		}
	}
	return report
}

func (st *CustomStarter) ReportError(rr error) {
	go func() {
		db, err := holders.Database()
		if err != nil { log.Error(err); return }
		builds := dbase.BuildsCollection(db)

		build := &project.Build{
			Uid: st.headers.RequestUid,
			Actor: st.event.Actor,
			Branch: st.event.Push.Changes[0].Old.Name,

			Error: rr,
			ErrorStr: rr.Error(),
		}

		err = builds.Insert(build)
		if err != nil { log.Error(err) }
	}()
}


func (st *CustomStarter) Clone() error {
	tempProj := &models.Project{Name:st.dbProj.Name, Uid: st.headers.RequestUid}
	st.base = path.Join(tempProj.Path(), "src")

	os.MkdirAll(st.base, os.ModeDir)
	_, err := st.session.ExecInDir(st.base,
		"git",
		"clone",
		st.dbProj.GitUrl,
		st.dbProj.Name,
	)
	if err != nil { return err }

	_, err = st.session.ExecInDir(path.Join(st.base, st.dbProj.Name), "git", "checkout", st.event.Push.Changes[0].Old.Name)
	if err != nil { return err }

	st.proj, err = models.NewProject(path.Join(st.base, st.dbProj.Name, ".buildable/project.json"))
	fmt.Println(st.proj, err)
	return err
}

func (st *CustomStarter) ReadProject() error {
	if st.proj == nil {
		return errors.New("could not get .buildable/project.json")
	}
	st.proj.Uid = st.headers.RequestUid
	st.proj.Name = st.dbProj.Name

	for _, b := range st.proj.Branches {
		if b.Name == st.event.Push.Changes[0].Old.Name {
			PIPELINES: for _, p := range b.Pipelines {
				if p == "" { continue PIPELINES }
				pipe, err := st.loadPipeline(b.Name, p)
				if err != nil {
					return err
				}
				st.proj.Pipelines.Store(p, pipe)
			}

			for i := len(b.Pipelines)/2-1; i >= 0; i-- {
				opp := len(b.Pipelines)-1-i
				b.Pipelines[i], b.Pipelines[opp] = b.Pipelines[opp], b.Pipelines[i]
			}

			if pipe, ok := st.proj.Pipelines.Load(b.Deploy); ok {
				st.proj.Deploy = pipe.(*pipelines.PipelineFile)
			} else {
				var err error
				st.proj.Deploy, err = st.loadPipeline(b.Name, b.Deploy)
				if err == nil {
					return err
				}
			}
		}
	}

	return nil
}

func (st *CustomStarter) loadPipeline(branch, filename string) (*pipelines.PipelineFile, error) {
	if _, ok := st.proj.Pipelines.Load(filename); ok { return nil, nil } // already parsed

	bytes, err := ioutil.ReadFile(path.Join(st.base, st.dbProj.Name, ".buildable", filename))
	if err != nil { return nil, errors.New(filename + ": " + err.Error()) }

	var pipe pipelines.PipelineFile
	err = yaml.Unmarshal(bytes, &pipe)
	if err != nil { return nil, errors.New(filename + ": " + err.Error()) }

	pipe.Name = filename
	return &pipe, nil
}