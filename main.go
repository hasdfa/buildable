package main

import (
	"buildable/server"
	"fmt"
	"runtime"
)

func main() {
	fmt.Println(runtime.GOOS)

	s := server.NewServer()
	s.StartServer()
}
